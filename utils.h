#if ! defined ( UTILS )
#define UTILS

#include "Individual.h"

// CONSTANTS VALUES
/*
    - Best genome (target)
*/
// Population = list of individual
/*
    - Constant size
    - Dynamic size
*/

static const bool debug = false;
static const unsigned long  BEST_GENOME = 0xFFFFFFFFFFFFFFFF;
static const int POPULATION_SIZE = 120;
static const int MAX_BITS_CHANGED_PER_MUTATION = 4;

static Individual* individual_list[POPULATION_SIZE];
static int last_killed_index = -1;
static bool target_generated = false;

unsigned long compute_fitness(unsigned long genome);
unsigned long distance_to_ref(unsigned long genome);

unsigned long mutate(unsigned long genome);

bool is_end();

void process_kill();

void process_generation();

void generate_population();
void refresh_fitness();

int get_min_fitness_index();
unsigned long get_min_fitness_value();
int get_max_fitness_index();
unsigned long get_max_fitness_value();

void print_individual_list();
void end_simulation();
void empty_population();


#endif