#include <iostream>
#include <bitset>
#include <random>
// #include <cmath>
#include <stdlib.h>
#include "utils.h"
using namespace std;

// Fitness 
/*
    - Distance from genome to other genome (absolute distance)
*/
unsigned long compute_fitness(unsigned long genome){
    return distance_to_ref(genome);
}

unsigned long distance_to_ref(unsigned long genome){
    unsigned long tmp = (BEST_GENOME > genome) ? BEST_GENOME - genome : genome - BEST_GENOME;
    return tmp;
    //return tmp/ (BEST_GENOME* 1.0);

    //return abs(BEST_GENOME - genome)/ BEST_GENOME;
    //return abs(BEST_GENOME - genome);
}

// Random modification of genome
/*
Proba que 1 ou 2 bits mutent par génération
    - Change genome (probability for each bit)
    - merge 2 existing genomes (2 individual => 1 new individual combination of 2 originals)
*/

unsigned long mutate(unsigned long genome){

    if(debug){cout << " MUTATION - from " << genome;}

    //Choose how many bits gets changed
    int nb_changed = rand() % MAX_BITS_CHANGED_PER_MUTATION + 1;

    for(int i = 0 ; i < nb_changed ; i++){
        //Choose which one
        int index = rand() % 64 + 1;
        //Change it
        unsigned long int unity = 1 ; // NEEDED !
        genome ^= (unity << index);
    }

    if(genome == BEST_GENOME){
        target_generated = true;
    }

    if(debug){cout << " to " << genome << endl;}

    return genome;
}




// Termination of the simulation
/*
    - Time out
    - All optimal genome
    - Existence of an optimal
*/
bool is_end(){
    if(debug){cout << " END test - target is generated : " << target_generated << endl;}
    return target_generated;
}


int get_min_fitness_index(){
    unsigned long curr_min_fit = individual_list[0]->fitness;;
    int min_index = 0;

    for(int n=0; n<POPULATION_SIZE; n++){
        if(individual_list[n]->fitness < curr_min_fit){
            curr_min_fit = individual_list[n]->fitness;
            min_index = n;
        }
    }

    if(debug){cout << " MIN index - index : " << min_index << " value : " <<  curr_min_fit << endl;}

    return min_index;
}

unsigned long get_min_fitness_value(){
    unsigned long curr_min_fit = individual_list[0]->fitness;
    int min_index = 0;

    for(int n=0; n<POPULATION_SIZE; n++){
        if(individual_list[n]->fitness < curr_min_fit){
            curr_min_fit = individual_list[n]->fitness;
            min_index = n;
        }
    }

    if(debug){cout << " MIN value - index : " << min_index << " value : " <<  curr_min_fit << endl;}

    return curr_min_fit;
}

int get_max_fitness_index(){
    unsigned long curr_max_fit = individual_list[0]->fitness;;
    int max_index = 0;

    for(int n=0; n<POPULATION_SIZE; n++){
        if(individual_list[n]->fitness > curr_max_fit){
            curr_max_fit = individual_list[n]->fitness;
            max_index = n;
        }
    }

    if(debug){cout << " MAX index - index : " << max_index << " value : " <<  curr_max_fit << endl;}

    return max_index;
}

unsigned long get_max_fitness_value(){
    unsigned long curr_max_fit = individual_list[0]->fitness;;
    int max_index = 0;

    for(int n=0; n<POPULATION_SIZE; n++){
        if(individual_list[n]->fitness > curr_max_fit){
            curr_max_fit = individual_list[n]->fitness;
            max_index = n;
        }
    }

    if(debug){cout << " MAX value - index : " << max_index << " value : " <<  curr_max_fit << endl;}

    return curr_max_fit;
}

// Selection = Death
/*
    - Tournament between 2 individual : best one survive (with/without probability)

    - Worst one killed
    - Random according fitness (mmin favorised)
    - Random (full)
*/
void process_kill(){

    //Choose the least appropriate individual
    int index = get_max_fitness_index();
    //int index = rand() % POPULATION_SIZE;

    if(debug){cout << " Kill process - index killed : " << index << " with fitness : " << individual_list[index]->fitness << endl;}

    last_killed_index = index; //Mark it as killed
    delete(individual_list[index]);

}
        
// Reproduction
/*
    - Best one survive and reproduce
    - Random according fitness (max favorised)
    - Random (full)
*/

void process_generation(){
    //Find the best individual
    int index = get_min_fitness_index();
    //int index = rand() % POPULATION_SIZE;

    //Mutate his genome
    unsigned long int tmp = mutate(individual_list[index]->genome);
    //assert(tmp!=individual_list[index]->genome)

    individual_list[last_killed_index] = new Individual(tmp);
    individual_list[last_killed_index]->set_fitness(compute_fitness(individual_list[last_killed_index]->genome));

    if(debug){cout << " Generation process - index source : " << index << " index replaced : " << last_killed_index << " new fitness : " << individual_list[last_killed_index]->fitness << endl;}
}

// Fill the population vector with individuals. Generate their starting genome.
void generate_population(){
      std::random_device rd;     //Get a random seed from the OS entropy device, or whatever
      const unsigned int seed = time(0)*time(0);
      cout << " Chosen generation seed : " << seed << " or rd : " << rd() <<  endl;
      std::mt19937_64 eng(rd()); //Use the 64-bit Mersenne Twister 19937 generator and seed it with entropy.
      //std::mt19937_64 eng(seed);

      //Define the distribution, by default it goes from 0 to MAX(unsigned long long)
      //or what have you.
      std::uniform_int_distribution<unsigned long> distr;

        // assert(individual_list.size == size);

      //Generate random numbers
      for(int n=0; n<POPULATION_SIZE; n++){

        unsigned long tmp = distr(eng);
        //PRINT
        std::bitset<64> b(tmp);
        cout << "Creation of : \t\t" << b << endl;

        //Add new individual to the list
        individual_list[n] = new Individual(tmp);

        individual_list[n]->print_genome();
      }


}

void end_simulation(){
    empty_population();
    target_generated = false;
    last_killed_index = -1;
}

// Fill the population vector with individuals. Generate their starting genome.
void empty_population(){
    //Generate random numbers
    for(int n=0; n<POPULATION_SIZE; n++){

        delete(individual_list[n]);
    }
}

// Fill the population vector with individuals. Generate their starting genome.
void refresh_fitness(){

    //Generate random numbers
    for(int n=0; n<POPULATION_SIZE; n++){
        individual_list[n]->set_fitness(compute_fitness(individual_list[n]->genome));
    }
}

// Print list of all individuals
void print_individual_list(){
    if(debug){cout << " List of individual : " << endl;}
    for(int n=0; n<POPULATION_SIZE; n++) {
        individual_list[n]->print();
    }

}
