#if ! defined ( INDIVIDUAL )
#define INDIVIDUAL

static unsigned int global_id = 0;

class Individual{
        //----------------------------------------------------------------- PUBLIC
    public:
        unsigned long genome;
        unsigned long fitness;
        unsigned int id;
        bool killed;

        Individual(unsigned long genome_tmp);

        void set_fitness(unsigned long fitness_tmp);
        void print_genome();
        void print();

    private :



};

#endif