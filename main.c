#include <iostream>

//#include "Individual.h"
#include "utils.h"
#include "Statistics.h"

using namespace std;

const int NB_ITER_PRINT = 10000;
const int NB_TIRAGE = 30000;
unsigned int iteration_needed[NB_TIRAGE];

int main(int argc, char **argv) {

    for (unsigned int t = 0; t < NB_TIRAGE; t++) {
        // Create new list of object
        cout << "Population generation ... " << endl;
        generate_population();
        refresh_fitness();

        print_individual_list();

        if (debug) { cout << "Simulation launch [press any key to launch]... " << endl; }

        // Until the end condition is reached
        unsigned int i = 0;
        for (i = 0; !is_end(); i++) { // and i <150
            if (debug) { cout << "Iteration # " << i << " ... " << endl; }

            if (i % NB_ITER_PRINT == 0 && i != 0) {
                cout << "Iteration # " << i << " Max fitness : " << get_max_fitness_value() << " at "
                     << get_max_fitness_index() << " Min fitness : " << get_min_fitness_value() << " at "
                     << get_min_fitness_index() << endl;
                //print_individual_list();
            }
            process_kill();
            process_generation();
            //refresh_fitness();
        }

        if (debug) { cout << "Simulation terminated." << endl; }
        cout << "Iteration # " << i << ". Best genome found." << endl;
        iteration_needed[t] = i;
        end_simulation();
    }


    cout << "End of the launch. Iterations needed : " << endl;
    Statistics stats_object = Statistics(iteration_needed, NB_TIRAGE);

    for (unsigned int t = 0; t < NB_TIRAGE; t++) {
        cout << iteration_needed[t] << " ";
    }
    cout << endl;

    cout << " Mean : " << stats_object.getMean() << " median : " << stats_object.median() << " stdDev : "
         << stats_object.getStdDev() << endl;

    return 0;
}

//std::bitset<8> genome (std::string("01111001"));
//std::bitset genome = 11110000;
//int genome = 0x11110000;
// identical: { B11111111, B11110000, B00001111, B00010001 };
