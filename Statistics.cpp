//
// Created by zettanux on 18/01/19.
//
#include <math.h>       /* sqrt */
#include "Statistics.h"
#include <algorithm>

Statistics::Statistics(unsigned int* data, unsigned int length) {
    this->data = data;
    size = length;
}

double Statistics::getMean() {
    double sum = 0.0;
    for(int i = 0 ; i < size; i ++){
        sum += this->data[i];
    }

    return sum/size;
}

double Statistics::getVariance() {
    double mean = getMean();
    double temp = 0;
    for(int i = 0 ; i < size; i ++){
        temp += (this->data[i]-mean)*(this->data[i]-mean);
    }
    return temp/(size-1);
}

double Statistics::getStdDev() {
    return sqrt(getVariance());
}

double Statistics::median() {
    //Arrays.sort(data);
    sort(data, data + size);

    if (size % 2 == 0) {
        return (data[(size/2) - 1] + data[size/2])/2.0;
    }
    return data[size/2];
}

