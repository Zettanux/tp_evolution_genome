#include <iostream>
#include <bitset>
#include <random>
#include "Individual.h"
using namespace std;

const bool debug = false;

// Individual definition
/*
    - Genome
    - Fitness
*/

// Data Members
unsigned long genome = 0xFFFFFFFFFFFFFFFF;
unsigned long fitness = 0;

// Constructor
Individual::Individual(unsigned long genome_tmp) {
    // Create new genome
    this->genome = genome_tmp;
    this->id = global_id;
    global_id += 1 ;

    if(debug){cout << "Individual created with " << genome_tmp << endl;}
}

// Member Functions()
void Individual::set_fitness(unsigned long fitness_tmp) {
    fitness = fitness_tmp;
}

void Individual::print_genome() {
    std::bitset<64> b(genome);

    if(debug){cout << " unsigned long : \t" << genome << endl;}
    if(debug){cout << " binary : \t\t" << b << endl;}
}

void Individual::print() {
    std::bitset<64> b(genome);
    if(debug){cout << "Individual #" << id << " \t fit= " << fitness << " \t genome= " << genome << " \t bin=" << b << endl;}
}


