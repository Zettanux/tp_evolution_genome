//
// Created by zettanux on 18/01/19.
//

#ifndef TP_EVOLUTION_GENOME_STATISTICS_H
#define TP_EVOLUTION_GENOME_STATISTICS_H

using namespace std;

class Statistics {

    public :
        unsigned int* data;
        int size;

        Statistics(unsigned int* data, unsigned int length);
        double getMean();
        double getVariance();
        double getStdDev();
        double median();
};

#endif //TP_EVOLUTION_GENOME_STATISTICS_H
